import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Suites from '../views/Suites.vue';
import Vendors from '../views/Vendors.vue';
import SuiteOps from '../views/SuiteOps.vue';
import VendorOps from '../views/VendorOps.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: '/',
        name: 'suites',
        component: Suites,
    },
    {
        path: '/vendors',
        name: 'vendors',
        component: Vendors,
    },
    {
        path: '/suite/:id',
        name: 'suiteData',
        component: SuiteOps,
        props: true,
    },
    {
        path: '/suite',
        name: 'suite',
        component: SuiteOps,
    },
    {
        path: '/vendor/:id',
        name: 'vendorData',
        component: VendorOps,
        props: true,
    },
    {
        path: '/vendor',
        name: 'vendor',
        component: VendorOps,
    },
];

const router = new VueRouter({
    routes,
});

export default router;
