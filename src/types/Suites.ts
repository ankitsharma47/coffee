import { Vendors } from './Vendors';

export interface Suites {
    id: string;
    name: string;
    logo: string;
    vendors: Vendors[];
}
