export interface Vendors {
    id: string;
    name: string;
    logo: string;
    vendorItems: VendorItems[];
}

export interface VendorItems {
    id: string;
    name: string;
    price: string;
}
