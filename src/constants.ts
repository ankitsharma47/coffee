const templates: string[] = [
    'Yoda best.',

    'May the thanks be with you.',

    'I thank you to the Death Star and back.',

    'When I needed help, you’re the one who knocks',

    'Thank you for making me feel Saul Good-man',

    'I don’t understand people who say, “I don’t know how to thank you!” Like they’ve never heard of pizza.',

    'If I had a cent for every time I appreciate you, I’d be a millionaire. Till then, let me buy you a coffee.',

    'I would thank you , but the Dothraki have no word for thank you.',

    'Not all heroes wear capes, some just hold the door.',

    'Winter is coming. So here’s some coffee.',
];
